require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const processHTMLPages = require('./processHTMLHelper.js');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const { argv } = require('yargs');

const extractCSS = new ExtractTextPlugin("[name].css", {
  allChunks: true
});

const plugins = [
  extractCSS
].concat(processHTMLPages());

const isProduction = !!((argv.env && argv.env.production) || argv.p);
const entries = [ './source/index.js' ];

if (! isProduction) {
  entries.push('webpack-dev-server/client?http://localhost:8080');
}

module.exports = {
  entry: entries,
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: [/\.scss$/i, /\.styl$/i, /\.css$/],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader?sourceMap', 'sass-loader?sourceMap', 'stylus-loader?sourceMap']
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(ttf|eot|svg|woff|woff2|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        query: {
          name: '[path][name].[ext]',
          context: './source'
        }
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.es6'],
  },
  output: {
    path: __dirname + '/build',
    filename: '[name].js',
    publicPath: '/'
  },
  devServer: {
    contentBase: './source'
  },
  plugins,
};
